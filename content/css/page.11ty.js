import fs from "fs";
import path from "path";
import postcss from "postcss";
import postcssImport from "postcss-import";
import autoprefixer from "autoprefixer";
import tailwindcss from "tailwindcss";
import cssnano from "cssnano";

const __dirname = import.meta.dirname;

export default class {
    async data() {
        const rawFilepath = path.join(
            __dirname,
            "..",
            "..",
            "_includes",
            "css",
            "_page.css",
        );

        return {
            permalink: `css/page.css`,
            rawFilepath,
            rawCss: fs.readFileSync(rawFilepath),
        };
    }

    async render({ rawCss, rawFilepath }) {
        const modules = [postcssImport, autoprefixer, tailwindcss];
        if (!process.env.DEBUG) {
            modules.push(cssnano);
        }

        const processor = postcss(modules);
        const result = await processor.process(rawCss, { from: rawFilepath });
        return result.css;
    }
}
