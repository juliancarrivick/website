import htmlmin from "html-minifier";
import yaml from "js-yaml";
import { EleventyHtmlBasePlugin } from "@11ty/eleventy";

/**
 *  @param {import("@11ty/eleventy/src/UserConfig")} eleventyConfig
 *  @returns {ReturnType<import("@11ty/eleventy/src/defaultConfig")>}
 */
export default async function (eleventyConfig) {
    // CUSTOM DATA
    eleventyConfig.addDataExtension("yaml", (contents) => yaml.load(contents));

    // FILTERS
    eleventyConfig.addFilter("toJson", (value) => JSON.stringify(value));

    // STATIC FILES
    eleventyConfig.addPassthroughCopy({
        "./static/": "/",
        "./node_modules/leaflet/dist": "/vendor/leaflet",
    });

    eleventyConfig.addWatchTarget("content/**/*.{svg|webp|png|jpeg}");
    eleventyConfig.addWatchTarget("content/css/");

    // PLUGINS
    eleventyConfig.addPlugin(EleventyHtmlBasePlugin);

    // TRANSFORM -- Minify HTML Output
    eleventyConfig.addTransform("htmlmin", function (content, outputPath) {
        if (outputPath && outputPath.endsWith(".html")) {
            let minified = htmlmin.minify(content, {
                useShortDoctype: true,
                removeComments: true,
                collapseWhitespace: true,
            });
            return minified;
        }
        return content;
    });
}

export const config = {
    dir: {
        input: "content",
        output: "public",
        data: "../_data",
        includes: "../_includes",
        layouts: "../_layouts",
    },
};
