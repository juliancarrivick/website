const colors = require("tailwindcss/colors");

module.exports = {
    content: [
        "./content/**/*.html",
        "./content/**/*.md",
        "./_includes/**/*.html",
        "./_includes/**/*.md",
        "./_layouts/**/*.html",
        "./_layouts/**/*.md",
    ],
    theme: {
        extend: {
            colors: {
                primary: colors.orange,
                accent: colors.pink,
            },
        },
    },
    plugins: [require("@tailwindcss/typography")],
};
